# What is Pandoc?

Pandoc is an universal document converter.
From [Pandoc's homepage](http://pandoc.org/):
> If you need to convert files from one markup into another, pandic is your
> swiss-army knife.
