# Issues lined out in Ioannidis (2014)

1. False and/or exaggerated results
Authors are under pressure to publish, and publish good results. Thus, some
authors tend to leave out caveats of their research, practice ~~p-hacking~~
data dredging, etc. If their data were freely accessible, they could be called
out on it. However, p-hacking is a consequence; the root of the problem is in
current publishing practices that need to change.
2. Science funding
If science is mainly funded by private investors, this leads to science being
more closed off, since it becomes a corporate asset.
3. What matters in science i.e. currency
Currently, the main currency in science is publications. The number of your
publications is often the way you are judged to be fit for a job, for example.
This leads back to the first issue: I need to publish, and only positive
results are published, therefor, if I don't have perfect results, I will make
them perfect myself.
