# The point of Fanelli (2013)
The author argues that the authors should be held responsible for what they
write and what they **don't** write. The problem is that misconduct now is
only discovered when someone questions the results and tries to re-run the
study. Also, what is considered misconduct, is manipulating the data or the
conditions. Fanelli (2013), however, argues that:
> ... mine points unambiguously to misconduct whenever there is a mismatch
> between what was reported and what was done.

Thus, his main point is that there should be no step missed in describing
what exactly was done to collect and analyse the data. 
