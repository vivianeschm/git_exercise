# What is Markdown?

An easy way to write your text. In essence, Markdown lets you write plain
text and add formatting to it easily. It is also readable with many different
programs. It is easy to add images and lists and the syntax is comprehensible.
See also this tweet:
![Fun with Word](https://pics.me.me/laurelhach-using-microsoft-word-moves-an-image-a-mm-to-3091957.png "Word tweet")
