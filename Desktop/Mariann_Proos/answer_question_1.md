# What is a Creative Commons license?

There are a few different sorts of Creative Commons licenses, all giving
slightly different access to the data/project in question. Generally,
the goal of the licenses is to make your material accessible to the public,
thereby improving on reproducibility of your project. The author **still retains
copyright**, but allows for non-commercial use of the materials.

For example, for my current project I have collected data from 70 experiment
participants and created mixed models to analyze the data. I could upload my
data and the R script to a platform together with a license. This way, anyone
could use the data I have collected and the script I have used to analyze it.
However, I would still have to be credited every time someone does use my
data.

*See also: [Creative Commons](https://creativecommons.org)*
