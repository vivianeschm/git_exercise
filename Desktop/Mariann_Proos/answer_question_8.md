#What is CSL?

CSL stands for Citation Style language. It is a language that enables formatting
citations/bibliographies.
In Pandoc, you can use CSL to create a bibliography automatically in your
preferred style.
