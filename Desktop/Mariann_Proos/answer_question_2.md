# What is the main difference between the different CC licenses?

The main differences lie in how the license allows others to use your
materials. The loosest licenses allow for non-commercial **and** commercial
use, and only require credit to be given to the author. You can also choose
a license that only allows for non-commercial use. Or a license that only
lets your materials be redistributed, but not used.
